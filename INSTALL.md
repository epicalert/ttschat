# Installing ttschat

## Dependencies

- nginx (or any web server capable of handling CGI scripts)
- libespeak
- python3
- pyttsx3
- ffmpeg

## Install instructions

1. Install dependencies.
2. Extract ttschat to a directory served by your web server.
3. Create the `rooms/` and `audio/` directories in the ttschat root. (Make sure they are writable by the web user!)
4. Create the `/var/lib/ttschat/usernames` directory and make it writable by the web user.
5. Configure your web server to handle requests to files in `api/` as CGI scripts.
