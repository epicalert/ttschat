var query = new URLSearchParams(document.location.search);

var room = query.get("room");

if (room == null) {
	newRoom() //create a new room if none was specified
}

var usernameKey = window.crypto.getRandomValues(new Uint32Array(1))[0];

var chat = document.getElementById("chat");

var messagesOld = [];

//send when enter key is pressed
$(document).keyup(function(e) {
	if($("#input").is(":focus") && e.key ==="Enter") {
		sendMessage();
	}
});

function refreshChat() {
	$.ajax({	
			url: "rooms/" + room + ".json",
			cache: false
		})
	.done(function(data) {
		$("#chat").html("");
		var messages = [];

		for (message in data) {
			//TODO: show icon when TTS not available for message
			$("#chat").append("<p><strong>" + data[message].user + ":</strong> " + escapeXml(data[message].text) + "</p>");

			if (messagesOld.indexOf(data[message].id) === -1 && messagesOld.length > 0) {
				var audio = new Audio("audio/" + data[message].id + ".ogg");
				audio.play();
			}

			messages.push(data[message].id);
		}

		messagesOld = messages;
	});
}

function sendMessage() {
	$.ajax({
		url: "api/send",
		method: "POST",
		data: { user: $("#username").val(),
			key: usernameKey,
			text: $("#input").val(),
			room: room
		}
	});
	$("#input").val("");
}

// https://stackoverflow.com/a/27979933 <3
function escapeXml(unsafe) {
    return unsafe.replace(/[<>&'"]/g, function (c) {
        switch (c) {
            case '<': return '&lt;';
            case '>': return '&gt;';
            case '&': return '&amp;';
            case '\'': return '&apos;';
            case '"': return '&quot;';
        }
    });
}

function newRoom() {
	$.ajax({
		url: "api/newroom",
	}).done(function(data) {
		document.location.search = "?room=" + data.room;
	});
}

window.setInterval(refreshChat, 3000);
